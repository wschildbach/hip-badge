# Hacking in Parallel Badge

## Logistics

The HiP Badge conforms to HS code is 847330.

## Contents

By default, the HiP Badge contains:

16 RGB LEDs on the edges
4 Simple LEDs in the middle
2 Input buttons
2 Special buttons
1 Lithium battery
1 USB connector
1 Charging circuit
1 Fuel guage
1 NFC EEPROM
1 NFC antenna

## Afterhacks

Some parts are missing from the badge, and are intended to be supplied and soldered by the badge owner.

1 UART connector
2 SAO connectors
1 Gas sensor
1 IR receiver
1 IR transmitter

## Features

The default application is delivered in a firmware binary, programmed on each badge at the factory.

### On-off switch

Each time the device is switched on, a fuel guage reading occurs. The edge LEDs illuminate briefly to indicate a full battery (blue), fresh battery (green), used battery (magenta), or low battery (red.)

### Battery charging

The small LEDs in the middle of the board indicate:

Battery is charging (blue blinking) from USB
Battery is connected to USB (green solid) but is full

### Fuel gauge

The fuel gauge is polled for battery charge levels.

The edge LEDs periodically (1 Hz) flash orange when the battery is low.

### Button input

Press the left and right buttons to switch the edge LED animations.

### NFC activity

Align the badge's antenna on the back to a NFC working smart phone.

When the badge reads NFC messages, the edge LEDs briefly flash green.

When the badge writes NFC messages, the edge LEDs briefly flash red.

### Gas activity

If the optional gas sensor is properly soldered, it will automatically be used.

The gas sensor is polled for CO2 levels.

The edge LEDs periodically (2 Hz) flash yellow on high levels of CO2.

### IR activity

If the optional infrared transceiver is properly soldered, it will automatically be used.

Align the badge's IR parts towards another badge.

When the badge reads IR messages, the edge LEDs briefly flash blue.

When the badge writes IR messages, the edge LEDs briefly flash pink.
