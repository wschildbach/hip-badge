Development is done on the v4.4.x branch of the ESP-IDF. If you have not
installed the ESP-IDF, please follow the excellent instructions at
https://docs.espressif.com/projects/esp-idf/en/v4.4.3/esp32c3/get-started/index.html

If you already have an installed ESP-IDF, check out the latest v4.4.x tag:

$ cd $IDF_PATH
$ git fetch
$ git checkout v4.4.3
(at this point you might have some residual files lying around in components/
 and examples/. You should very carefully inspect each and everyo NOOOO!!!!!!
 Nuke them from orbit: rm -rf components/ examples/; git checkout .)
$ git submodule update --init --recursive
$ bash install.sh

To get started with your HiP Badge on a computer with a ESP-IDF installation:

$ cd HiP-Badge/firmware
$ git submodule update --init --recursive
$ . /path/to/your/esp-idf/export.sh
$ idf.py set-target esp32c3
$ idf.py menuconfig --help (appearance with --style)
$ idf.py menuconfig (generates a sdkconfig file)

$ idf.py add-dependency hip_badge_v1

$ idf.py -p PORT erase-flash
$ idf.py -p PORT erase-otadata
